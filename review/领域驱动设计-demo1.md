##### 1.Domain primitive

领域驱动设计的驱动是促进促使的意思，可以理解为基于领域的工程设计，那什么是领域呢？我们暂且可以将其理解为业务问题的范畴，领域可大可小，对应着大小业务的问题边界，简单来说，领域驱动设计就是将业务上要做的一件大事通过推演和抽象，拆分成多个内聚的领域。这不就是模块化和微服务吗，其实不贴切，无论是模块化，微服务，还是其他各种方法论和工程实践，只能认为他们都是为了达成降低软件开发维护复杂度这个目的而遵循了解耦的原则。他们属于软件开发中不同层面的实现方式。

![image-20230202224254999](C:\Users\20127\AppData\Roaming\Typora\typora-user-images\image-20230202224254999.png)

~~~java
public class User {
    Long userId;
    String name;
    String phone;
    Long repId;
}

public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepRepository salesRepRepo;
    private UserRepository userRepo;

    public User register(String name, String phone)
            throws ValidationException {
        // 参数校验
        if (name == null || name.length() == 0) {
            throw new ValidationException("name");
        }
        if (phone == null || !isValidPhoneNumber(phone)) {
            throw new ValidationException("phone");
        }

        // 获取手机号归属地编号和运营商编号 然后通过编号找到区域内的分组编码
        String areaCode = getAreaCode(phone);
        String operatorCode = getOperatorCode(phone);
        SalesRep rep = salesRepRepo.findRep(areaCode, operatorCode);

        // 最后创建用户，落盘，然后返回
        User user = new User();
        user.name = name;
        user.phone = phone;

        if (rep != null) {
            user.repId = rep.repId;
        }

        return userRepo.save(user);
    }

    private boolean isValidPhoneNumber(String phone) {
        String pattern = "^0[1-9]{2,3}-?\\d{8}$";
        return phone.matches(pattern);
    }
    
    private String getAreaCode(String phone) {
        //...
    }
    
    private String getOperatorCode(String phone) {
       //...
    }
}
~~~

代码审视：

1.接口语义明确，可扩展性强，最好带有自检性

原本问题：接口语义不明确的原因：使用了多个相同基本类型的参数，可扩展性不强的原因是使用了基本类型和参数个数写死

~~~java
public User register(String name,String phone);

// 签名改动
public User registerByIdCard(String name,String idCard);

public User registerByPhoneAndIdCard(String name,String phone,String idCard);

~~~

解决办法：使用自定义类型，将对属性的校验逻辑封装到这个自定义类型中

2.参数校验逻辑复用，内聚，不要分散在各个方法中

原本问题：方法内一开始对参数进行合法性校验，如果存在多个类似的方法，那么每个方法都要在开头进行校验，存在大量重复代码，而且一旦某种类型的参数校验逻辑需要修改，那么每个地方都要一一修改。使用校验工具类可以解决部分问题，但是在业务方法中还是需要主动调用参数校验工具类，且参数校验异常与业务逻辑异常依然耦合。

3.参数校验异常和业务逻辑异常解耦

改造后代码：

~~~java
public User register(String name, PhoneNumber phone)

public class PhoneNumber {
    private final String number;
    private final String pattern = "^0?[1-9]{2,3}-?\\d{8}$";
    
    public String getNumber() {
        return number;
    }

    // 仅存在含参构造器
    public PhoneNumber(String number) {
        if (number == null) {
            throw new ValidationException("number不能为空");
        } else if (isValid(number)) {
            throw new ValidationException("number格式错误");
        }
        this.number = number;
    }

    private boolean isValid(String number) {
        return number.matches(pattern);
    }

}
~~~

在构建PhoneNumber对象时，就会执行校验逻辑，确保被构建出来的对象一定是合法的

方法签名中使用自定义类型，在编译器会进行强类型校验，避免传参乱序的问题

~~~java
public class User {
    Long userId;
    String name;
    PhoneNumber phone;
    Long repId;
}

public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepRepository salesRepRepo;
    private UserRepository userRepo;

    public User register(String name, PhoneNumber phone) {
    
        // 获取手机号归属地编号和运营商编号，然后通过编号找到区域内的分组编码
        String areaCode = getAreaCode(phone);
        String operatorCode = getOperatorCode(phone);
        SalesRep rep = salesRepRepo.findRep(areaCode, operatorCode);

        // 最后创建用户，落盘，然后返回
        User user = new User();
        user.name = name;
        user.phone = phone;

        if (rep != null) {
            user.repId = rep.repId;
        }

        return userRepo.save(user);
    }
    
    private String getAreaCode(PhoneNumber phone) {
        //...
    }
    
    private String getOperatorCode(PhoneNumber phone) {
       //...
    }
}
~~~

RegistrationServiceImpl承担的是注册功能，注册最本质的行为是拿到用户信息并存储，保证业务逻辑的简洁易读

~~~java
// 获取手机号归属地编号和运营商编号
String areaCode = getAreaCode(phone);
String operatorCode = getOperatorCode(phone);
~~~

这两个行为放在注册这个业务域内并不合适，什么逻辑应该归属于哪个业务域。类似于如何对微服务进行边界限定。

这两个行为都是获取手机号相关的属性，应该内聚在手机号这个类型中，在抽象上才是合理的。

~~~java
public class PhoneNumber {

    private final String number;
    private final String pattern = "^0?[1-9]{2,3}-?\\d{8}$";
    
    public String getNumber() {
        return number;
    }

    // 仅存在含参构造器
    public PhoneNumber(String number) {
        if (number == null) {
            throw new ValidationException("number不能为空");
        } else if (isValid(number)) {
            throw new ValidationException("number格式错误");
        }
        this.number = number;
    }

    private boolean isValid(String number) {
        return number.matches(pattern);
    }
    
    public String getAreaCode() {
        //...
    }
    
    public String getOperatorCode(PhoneNumber phone) {
       //...
    }
}
~~~

优化完以后的业务逻辑方法变为：

~~~java
public class User {
    Long userId;
    String name;
    PhoneNumber phone;
    Long repId;
}

public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepRepository salesRepRepo;
    private UserRepository userRepo;

    public User register(String name, PhoneNumber phone) {
    
        // 获取用户信息
        SalesRep rep = salesRepRepo.findRep(phone.getAreaCode(), phone.getOperatorCode());

        // 存储用户信息
        User user = new User();
        user.name = name;
        user.phone = phone;

        if (rep != null) {
            user.repId = rep.repId;
        }

        return userRepo.save(user);
    }
}
~~~

该案例比较简单，即使获取手机号归属地编号和运营商编号这两个方法耦合在注册业务中也并没有对可维护性造成很多的破坏，但是遇到复杂的业务场景改动起来就会比较困难。

DDD与传统MVC开发的差异点之一：

传统POJO类只包含属性值get/set方法，属于贫血模型，而像phoneNumber不仅包含属性，还拥有与其属性相关的职责，是充血模型。

像该案例中的phoneNumber，我们将这种类型称为Domain primitive，在DDD中，DP是一切模型、方法、架构的基础。

它是在特定领域，拥有精准定义，可以自我验证，拥有行为的对象。可以认为是领域的最小组成部分。

DP三条原则：

①让隐性的概念显性化

归属地编号，运营商编号属于电话号码这个事物的隐性属性， 如果使用String类型定义电话号码，那么这些隐性属性就很难体现出来。

在phoneNumber中我们赋予它行为来让隐性的概念显性化

②让隐性的上下文显性化

~~~java
public class PhoneNumber {

    private final String number;
    private final String protocol；// 区号协议，比如：E.123
~~~

全世界的手机号的区号分配都是根据国际电信联盟（ITU）分配的E.123和E.164标准所分配的。在不同的区号分配协议下，相同的数字代表的含义是不同的，区号协议就是手机号字符串的隐性上下文，PhoneNumber类中加上protocol属性，就是让隐性的上下文显性化

③封装多对象行为

一个DP可以封装其他多个DP的行为

##### 2.entity，domain service

DP就是构建领域的基础类型

UserRepository概念还未带出，回退到无Repository版本

~~~java
public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepMapper salesRepDAO;
    private UserMapper userDAO;

    public UserDO register(String name, PhoneNumber phone) {
    
        // 获取用户信息
        SalesRepDO repDO = salesRepDAO.select(phone.getAreaCode(), phone.getOperatorCode());

        // 存储用户信息
        UserDO userDO = new UserDO();
        userDO.name = name;
        userDO.phone = phone;

        if (repDO != null) {
            userDO.repId = repDO.repId;
        }

        return userDAO.insert(userDO);
    }
}
~~~

需求场景

![image-20230205212628152](C:\Users\20127\AppData\Roaming\Typora\typora-user-images\image-20230205212628152.png)

初代 写法：

~~~java
public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepMapper salesRepDAO;
    private UserMapper userDAO;
    private RewardMapper rewardDAO;
    private TelecomRealnameService telecomService;
    private RiskControlService riskControlService;

    public UserDO register(String name, PhoneNumber phone) {
        // 参数合法性校验已在PhoneNumber中处理
        // 用手机号查询（三方提供的手机号查询实名信息服务，如：中国电信）
        TelecomInfoDTO rnInfoDTO = telecomService.getRealnameInfo(phone.getNumber());
        if (!name.equals(rnInfoDTO.getName())) {
            throw new InvalidRealnameException();
        }
       
        // 按照一定逻辑计算得出该用户的标签，该标签将作为用户的一个属性
        String label = getLabel(rnInfoDTO);
        
        // 根据手机号的归属地和所属运营商，查询得到关联的销售组信息，该销售组ID将作为用户的一个属性
        String salesRepId = getSalesRepId(phone);
        
        // 构造User对象和Reward对象
        String idCard = rnInfoDTO.getIdCard();
        UserDO userDO = new UserDO(idCard, name, phone.getNumber(), label, salesRepId);
        RewardDO rewardDO = RewardDO(idCard, label);
        
        // 检查风控
        if(!iskControlService.check(idCard, label)) {
            // 若不通过，用户保持新客身份，但查询不到福利信息
            userDO.setNew(true);
            rewardDO.setAvailable(false);
        }else {
            // 若通过，用户失去新客身份，且可以查询到福利信息
            userDO.setNew(false);
            rewardDO.setAvailable(true);
        }
        
        // 存储信息
        rewardDAO.insert(rewardDO);
        return userDAO.insert(userDO);
    }
    
    private String getLabel(TelecomInfoDTO dto) {
        // 本地逻辑处理
    }
    
    private String getSalesRepId(PhoneNumber phone) {
        SalesRepDO repDO = salesRepDAO.select(phone.getAreaCode(), phone.getOperatorCode());
        if (repDO != null) {
            return repDO.getRepId();
        }
        return null;
    }
}
~~~

代码审视：

1.对外部依赖的耦合非常严重。一切不属于当前域内的设施和服务，都可以被认为是外部依赖，比如数据库，数据库表，RPC服务，ORM框架，中间件，它们还有一个特征，这些依赖是可被替换的。因为这些都是属于业务领域核心逻辑之外的东西，对他们是没有控制权的，所以要做的是，即便这些外部依赖发生了变化，也能将自己系统内所产生的变动控制在最小的范围内，由外部依赖的变化所导致的内部系统的改造程度，可以理解为一个系统的可维护性。

造成上述耦合的主要原因是：面向具体实现编程。改造思路为：面向抽象接口编程

第一步：改造RPC服务调用

~~~java
public interface RealnameService {
    RealnameInfo get(PhoneNumber phone);
}

public class TelecomRealnameService implements RealnameService {

    @Override
    public RealnameInfo get(PhoneNumber phone){
        // RPC调用，并将返回结果封装为RealnameInfo
        // RealnameInfo是DP
    }
}
~~~

~~~java
public class RegistrationServiceImpl implements RegistrationService {

    private SalesRepMapper salesRepDAO;
    private UserMapper userDAO;
    private RewardMapper rewardDAO;
    private RealnameService realnameService;
    private RiskControlService riskControlService;

    public UserDO register(String name, PhoneNumber phone) {
        // 一致性校验
        RealnameInfo realnameInfo = realnameService.get(phone);
        realnameInfo.check(name);
       
        // 计算标签信息
        String label = getLabel(realnameInfo);

        // 计算销售组
        String salesRepId = getSalesRepId(phone);

        // 构造对象
        String idCard = realnameInfo.getIdCard();
        UserDO userDO = new UserDO(idCard, name, phone.getNumber(), label, salesRepId);
        RewardDO rewardDO = RewardDO(idCard, label);
        
        // 检查风控
        if(!riskControlService.check(idCard, label)) {
            userDO.setFresh(true);
            rewardDO.setAvailable(false);
        }else {
            userDO.setFresh(false);
            rewardDO.setAvailable(true);
        }
        
        // 存储信息
        rewardDAO.insert(rewardDO);
        return userDAO.insert(userDO);
    }
    
    private String getLabel(RealnameInfo info) {
        // 本地逻辑处理
    }
    
    private String getSalesRepId(PhoneNumber phone) {
        SalesRepDO repDO = salesRepDAO.select(phone.getAreaCode(), phone.getOperatorCode());
        if (repDO != null) {
            return repDO.getRepId();
        }
        return null;
    }
}
~~~

1.实名查询服务，使用依赖倒置（设计代码结构时，高层模块不应该依赖低层模块，二者都应该依赖其抽象）

2.实名信息，使用DP抽象，使用RealnameInfo代替TelecomInfoDTO，后续的业务逻辑只需要处理RealnameInfo，并不关心是哪个具体服务返回的结果，并且DP具有逻辑内聚的好处，这样无论外部服务是参数变化还是替换实现，都将变动范围控制在了具体实现类和配置文件内部，能够保证核心业务逻辑的稳定。

第二步：外部依赖-数据访问对象的改造

DO作为数据表的直接映射，属于具体实现，不应该直接暴露给业务逻辑，DAO作为访问数据库的具体实现，也不应该暴露给业务。

由此引入领域驱动设计中的Entity和Repository，老代码面向DO和DAO编程，事实上业务逻辑应该只面向领域实体，而并不需要关心这个对象背后是否用到了数据库或者什么数据库

~~~java
// User Entity
public class User {
    // 用户id，DP
    private UserId userId;
    // 用户手机号，DP
    private PhoneNumber phone;
    // 用户标签，DP
    private Label label;
    // 绑定销售组ID，DP
    private SalesRepId salesRepId;
    
    private Boolean fresh = false;
    
    private SalesRepRepository salesRepRepository;
    
    // 构造方法
    public User(RealnameInfo info, name, PhoneNumber phone) {
        // 参数一致性校验，若校验失败，则check内抛出异常（DP的优点）
        info.check(name);
        initId(info);
        labelledAs(info);
        // 查询
        SalesRep salesRep = salesRepRepository.find(phone);
        this.salesRepId = salesRep.getRepId();
    }
    
    // 对this.userId赋值
    private void initId(RealnameInfo info) {
    
    }
    
    // 对this.label赋值
    private void labelledAs(RealnameInfo info) {
        // 本地处理逻辑
    }
    
    public void fresh() {
        this.fresh = true;
    }
}
~~~

User，领域实体类，User类中的属性用于描述在这个系统内客户应该含有的信息，在其中尽量多使用DP将更多的自检和隐性属性内聚起来，在定义User类时，不用关心下层数据库要怎么实现，甚至之后User对象的一部分信息存储在内存中，一部分存储在数据库中也是无所谓的。

Entity和DP的区别：

Entity：有状态，领域实体

DP：无状态，组成实体的基础类型

“状态”：该对象是否存在生命周期，程序是否需要追踪该对象的变化事件

接下来看数据库的具体实现，业务逻辑不应该耦合数据访问的实现。

Repository就是数据访问的抽象层，在抽象层，我们只定义领域实体的动作，在UserRepository的实现类中，就可以依赖数据库的各种具体实现，比如mybatis或者redisClient。

~~~java

public interface UserRepository {
    User find(UserId id);
    User find(PhoneNumber phone);
    User save(User user);
}

public class UserRepositoryImpl implements UserRepository {

    private UserMapper userDAO;

    private UserBuilder userBuilder;
    
    @Override
    public User find(UserId id) {
        UserDO userDO = userDAO.selectById(id.value());
        return userBuilder.parseUser(userDO);
    }

    @Override
    public User find(PhoneNumber phone) {
        UserDO userDO = userDAO.selectByPhone(phone.getNumber());
        return userBuilder.parseUser(userDO);
    }

    @Override
    public User save(User user) {
        UserDO userDO = userBuilder.fromUser(user);
        if (userDO.getId() == null) {
            userDAO.insert(userDO);
        } else {
            userDAO.update(userDO);
        }
        return userBuilder.parseUser(userDO);
    }
}
~~~

通过对数据访问层的改造，业务代码变为：

核心业务逻辑不再依赖任何具体实现，无论什么外部依赖发生改动，只需要去修改相应的具体实现类，这样业务就比较稳定。

~~~java
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private RewardRepository rewardRepository;
    private RealnameService realnameService;
    private RiskControlService riskControlService;

    public UserDO register(String name, PhoneNumber phone) {
        // 查询实名信息
        RealnameInfo realnameInfo = realnameService.get(phone);

        // 构造对象
        User user = new User(realnameInfo, phone);
        Reward reward = Reward(user);
        
        // 检查风控
        if(!riskControlService.check(user)) {
            user.fresh();
            reward.inavailable();
        }
        
        // 存储信息
        rewardRepository.save(reward);
        return UserRepository.save(user);
    }
}
~~~

内部逻辑耦合改造：

register方法中耦合了各种各样的业务逻辑，比如参数一致性校验，计算用户标签，查询绑定的销售组信息，检查风控，存储用户信息等，很多逻辑并不属于注册这个业务需要关心的。

奖品对象和风控检查这种发奖逻辑耦合在了注册逻辑中，发奖的原因是为了给予新用户一些福利，本质是判断当前用户是否为新用户而衍生出来的操作，在注册这个业务域中，我们将它的行为抽象为获取用户信息，检查并更新用户信息，存储用户信息这三个步骤。而检查并更新用户信息这个逻辑中，存在发奖这个衍生行为，改造如下：

~~~java
public interface CheckUserService(User user) {
    void check(user);
}

public class CheckAndUpdateUserServiceImpl(User user) {
    private RiskControlService riskControlService;
    private RewardRepository rewardRepository;
    
    @Override
    public void check(User user) {
        rewardCheck(user);
        // ...
        // 可能存在的其他逻辑
    }
    
    private void rewardCheck(User user) {
        Reward reward = Reward(user);
        // 检查风控
        if(!riskControlService.check(user)) {
            user.fresh();
            reward.inavailable();
        }
        rewardRepository.save(reward);
    }
}
~~~

checkUserService里面可能会改变User对象和Reward对象的状态，涉及到多个Entity状态改变的服务，被称为domain service，domain service主要用于封装多Entity或者跨业务域的逻辑

~~~java
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private RealnameService realnameService;
    private CheckUserService checkUserService;

    public UserDO register(String name, PhoneNumber phone) {
        // 查询信息
        RealnameInfo realnameInfo = realnameService.get(phone);

        // 构造对象
        User user = new User(realnameInfo, phone);
        
        // 检查并更新对象
        checkUserService.check(user);
        
        // 存储信息
        return userRepository.save(user);
    }
}
~~~

归纳总结：

DP：抽象并封装自检和一些隐性属性的计算逻辑，且这些属性是无状态的

Entity：抽象并封装单对象有状态的逻辑

Domain service：抽象并封装多对象的有状态逻辑

Repository：抽象并封装外部数据访问逻辑

1.首先对需要处理的业务问题进行总览

2.然后领域对象（Entity）进行划分，明确每个领域对象包含的信息和职责边界

3.接着在上层应用中根据业务描述去编排Entity和Domain Service

4.对下层数据进行访问，RPC调用去做一些实现

##### 3.统一语言和模型价值

DP也可以被称为是value object，

##### 4.聚合根&界限上下文

案例：

~~~wiki
假设微信账号的主键是手机号，现在用户A想要注销自己的手机号，那么系统是否通过A的手机号删除关联的微信钱包信息？如果不需要，运营商在回收了A的手机号之后将手机号转发给B，B在使用该手机号注册微信号，则已经绑定了其他人的微信钱包信息，如果删除钱包，同理是否需要删除绑定在钱包中的银行卡信息，如果银行卡不仅绑定在了A的账号里，还作为亲属卡绑定在了用户A的儿子的账号里，那么删除这张银行卡的绑定信息是否会影响到用户A的儿子的钱包使用？
~~~

在一个复杂的系统中，对一个对象的修改，可能会涉及到大量其他关联对象的状态，如何使这些对象的状态始终保持一致？

Aggregate（聚合）：描述这种存在引用关系的对象集合

[![rmRbq.png](https://i.328888.xyz/2023/02/06/rmRbq.png)](https://imgloc.com/i/rmRbq)

聚合的目的是屏蔽掉内部对象之间复杂的关联关系，只对外暴露统一的接口。

根对象：整个聚合中唯一能被外部引用的对象，即聚合所暴露的接口，只允许操作根对象（根对象是一个Entity）

边界：判断哪些对象可以被放入当前聚合的条件

[![rmBCa.png](https://i.328888.xyz/2023/02/06/rmBCa.png)](https://imgloc.com/i/rmBCa)

注销账号涉及到了账号信息，钱包信息，钱包信息又关联银行卡信息。

当前聚合是为了解决账号注销的问题

聚合外部可以持有根对象的引用来操作聚合，而聚合内部通过执行一系列逻辑来保持各个对象状态的一致。

案例demo：用户A转账给用户B

~~~java
// 用于所有作为标识的DP的公共父接口
public interface Identifier extends Serializable {
}

// 该泛型指定了Entity的标识类型，也就是实现了Identifier接口的DP
public interface Entity<T extends Identifier> {

}

// 手机号可以作为一些账号的唯一标识
public class PhoneNumber implements Identifier {
    
}

// 聚合根是Entity，聚合根也需要一个标识
public interface AggregateRoot<T extends Identifier > extends Entity<T>{
    
}

// 手机号是每个账号的全局标识，WechatAccount充当被外部引用的根
public class WechatAccount implements AggregateRoot<PhoneNumber> {
    
}
~~~

每个entity都是存在标识的，标识一般是不可变类型

该场景涉及到三种类型的对象：

~~~java
public class Balance implements Entity<BalanceNumber> {
    private BalanceNumber id;
    //...
    private BalanceNumber() {}
}

public class Wallet implements Entity<WalletNumber> {
    private WalletNumber id;
    private Balance balance; // 余额
    private Statement statement； // 账单明细
    //...
    private Wallet() {}
}

// 聚合根
public class WechatAccount implements AggregateRoot<PhoneNumber> {
    private PhoneNumber id;
    private Nickname nickname; 
    private Wallet wallet;
    //...
    private WechatAccount() {}
}
~~~

用户A转账给用户B，涉及到多个entity的状态变化，这些变化的对象就是一个聚合。

~~~java
// domain service
public class TransferServiceImpl {
    // 包含多种更新数据的操作，如更新数据库表或者刷新缓存
    public AccountRepository repository;
    
    @Override
    public void transfer(WechatAccount payer, WechatAccount payee, Asset asset) {
        payer.withdraw(asset);
        payee.deposit(asset);
        repository.save(payer);
        repository.save(payee);
   }
}
~~~

WechatAccount作为聚合根，它的引用能被外部获取并保持，而钱包对象处于聚合的内部，它的引用只能被内部持有

通过操作聚合根来处理业务过程中的聚合中所有关联对象在运行时的状态一致

~~~java
public class WechatAccount implements AggregateRoot<PhoneNumber> {
    private PhoneNumber id;
    private Nickname nickname; 
    private Wallet wallet;
    //...
    private WechatAccount() {}
    
    public void withdraw(Asset asset) {
        wallet.pay(asset);
    }
    
    public void deposit(Asset asset) {
        wallet.receive(asset);
    }
}

public class Wallet implements Entity<WalletNumber> {
    private WalletNumber id;
    private Balance balance; // 余额
    private Statement statement； // 账单明细
    //...
    private Wallet() {}
    
    public void pay throws BalanceException(Asset asset) {
        balance.decrease(asset.getBalance);
        statement.add(asset);
    }
    
    public void receive throws BalanceException(Asset asset) {
        balance.increase(asset.getBalance);
        statement.add(asset);
    }
    
} 
~~~

Repository作为数据操作组件，屏蔽了上层领域逻辑和下层的数据库实现，通过Repository只操作聚合根来实现关联对象的在持久化时的状态一致

~~~java
// 聚合根子类及聚合根标识
public interface Repository<T extends AggregateRoot<ID>, ID extends Identifier> {
    save(T t);
    WechatAccount find(ID id);
}

public interface AccountRepository extends Repository<WechatAccount, PhoneNumber> {
    // 持久化当前聚合中所有对象的状态
    save(WechatAccount wechatAccount);
    // 通过手机号查找整个聚合
    WechatAccount find(PhoneNumber phoneNumber)
}
~~~

Entity是对业务领域的建模，而不是数据库表的映射

~~~java
public class WeAccountRepositoryImpl implements AccountRepository {
    private WechatAccountDAO wechatAccountDAO;
    private WalletDAO  walletDAO;
    private BalanceDAO balanceDAO;
    private StatementDAO statementDAO;
    
    @Override
    public save(WechatAccount wechatAccount) {
        WechatAccountPO wechatAccountPO = WechatAccountConverter.convert(wechatAccount);
        wechatAccountDAO.save(wechatAccountPO);
        
        Wallet wallet = WalletFactory.obtain(wechatAccount);
        WalletPO walletPO = walletConverter.convert(wallet);
        walletDAO.save(walletPO);
        
        Balance balance = BalanceFactory.obtain(wallet);
        BalancePO balancePO = balanceConverter.convert(balance);
        balanceDAO.save(balancePO);
        
        // 账单明细表
        Statement statement = StatementFactory.obtain(wallet);
        StatementPO statementPO = statementConverter.convert(statement);
        statementDAO.save(statementPO);
        
        //... 其他WechatAccount的属性
    }
    
    @Override
    WechatAccount find(PhoneNumber phoneNumber) {
        //...
    }
}
~~~

在一个复杂的软件中，一个业务动作会涉及大量存在关联的对象，聚合的价值就是通过封装，保持所有关联对象的状态一致。

在运行时，领域方法通过持有聚合根维护了对象的状态一致，在持久化时，Repository通过持有聚合根来维持数据一致。

限界上下文：解决复杂系统中的领域分治问题

